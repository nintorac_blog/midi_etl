# MIDI extraction pipeline on a modern data platform

[See the related blog post](https://www.nintoracaudio.dev/data-eng/2022/01/07/midi-etl.html)


## Setup

1. Run the following commands in shell
```bash
mkdir .dagster              # create the dagster home directory
cp dagster.yaml .dagster    # copy in the dagster config
docker-compose up -d        # start all the infrastructure
```
1. Check [Dagit](http://localhost:3000)
3. Check [Minio](http://localhost:9001) 
    - username: minio
    - password: minio123
    - In minio you will need to create a bucket named `midi_etl`
4. Check [Trino](http://localhost:8080) - username anything eg `trino`
    - Trino requires you to create schema named `midi`
    - execute the statements to create the schemas
```sql
CREATE SCHEMA minio.midi WITH (LOCATION = 's3a://midietl/midi');
CREATE SCHEMA minio.midi_detailed WITH (LOCATION = 's3a://midietl/midi_detailed');
CREATE SCHEMA minio.midi_music WITH (LOCATION = 's3a://midietl/midi_music');
CREATE SCHEMA minio.midi_standard WITH (LOCATION = 's3a://midietl/midi_standard');
```
	- Execute this query to verify `SHOW SCHEMAS IN minio;`. You should get
```
default
information_schema
midi
midi_detailed
midi_music
midi_standard
```
    - If you do not have a [database IDE](https://dbeaver.io/) already you can get a trino shell with `docker run -it --rm  --name trino --network midietl trinodb/trino trino --server trino-coordinator:8080`. From there you can enter the query.

## Build

This is a reproduction of the instructions in the [article](https://www.nintoracaudio.dev/data-eng/2022/01/07/midi-etl.html), refer there for more context

1. In the jobs menu click `fetch_raw` 
	1. shift + click the `Materialize All` button on the top right of the DAG canvas
	2. Make sure the config entry `ops.lmd_full_bytes.config.sample` is `true`, this will download a single prefix from Lakh. Set to `false` to download the entire dataset. Note it just so happens that in Lakh the first prefix is `f`
1. In the jobs menu click `index_midi` 
	1. click the `Materialize All` button on the top right of the DAG canvas (no shift this time!)
	2. In the textbox supply this value `f0, f1, f2, f3, f4, f5, f6, f7, f8, f9, fa, fb, fc, fd, fe, ff`, this will process and index all of the sample MIDI files into parquet files
2. In the jobs menu click `run_trino_refresh_job`
	1. click into the `launchpad` tab in the top left (just to the right of the jobs menu)
	2. click `launch run` in the bottom right corner, this runs some sync calls on the relevant tables that tells Trino to update the Hive metastore
2. In the jobs menu click `dbt`
	1. Click `Materialize All`
3. In the jobs menu click `analysis`
	1. Due to [this issue](https://github.com/starburstdata/dbt-trino/issues/226) it is neccesary to run this command first `docker run --network host -it --entrypoint "/bin/sh" minio/mc -c "mc config host add dc http://localhost:9000 minio minio123 && mc rm dc/midietl/midi_standard/program_information/"`
	2. Click `Materialize All`
	3. Once the job completes click on the `basic_analysis` asset and then on the right hand panel click `notebook` - TODO - to see some initial exploration of the data.


## Useful commands

### Trino is bootlooping with a spill error

Run `docker-compose stop trino-coordinator  && docker rm lakh_dataset_trino-coordinator_1  && docker-compose up -d`, this will clear the data in the spill directory and allow Trino to start